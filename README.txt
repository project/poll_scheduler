The Name of My Project
======================

Activates Polls on the date they are published by Scheduler module.

Scheduler module can be used to set a date when content goes from the
'unpublished' state to the 'published' state. If that content happens to be
a poll, you might also want the poll to activate on that same date.


Depends on:

* Poll module
* Scheduler module


Installation
------------

- Install this module using the official instructions at
  http://drupal.org/documentation/install/modules-themes/modules-7

- Polls will now also be activated when Scheduler publishes them.


Issues
------

Bugs and Feature requests should be reported in the Issue Queue:
https://www.drupal.org/project/issues/poll_scheduler?categories=All

License
-------

This project is GPL v2 software. See the LICENSE.txt file in this directory for
complete text.
